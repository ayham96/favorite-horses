import numpy as np

from client import ATGClient

class favoritesService:
    def __init__(self, atgClient: ATGClient):
        self._atgClient = atgClient

    def summarize_last_3_games(self, game_type):
        games = self._atgClient.get_games_by_type(game_type)
        last_3_games = list(sorted(games["results"], key=lambda x: x["startTime"], reverse=True))[:3]

        game_summaries = []
        for game in last_3_games:
            game_details = self._atgClient.get_game_by_id(game["id"])
            game_summaries.append(self.summarize_game(game_details))

        return game_summaries

    def summarize_game(self, game_details):
        game_id = game_details["id"]
        races = [self.summarize_race(r) for r in game_details["races"]]
        favorite_horse_finish_orders = list(map(lambda x: x["favorite_horse_finish_order"], races))
        median_finish_order = np.median(favorite_horse_finish_orders)
        favorite_horse_win_total = sum(map(lambda x: x["favorite_horse_winned"], races))
        favorite_horse_lost_total = sum(map(lambda x: not x["favorite_horse_winned"], races))
        favorite_horse_winning_ratio = favorite_horse_win_total/len(races)

        return {
            "game_id": game_id,
            "races": races,
            "statistics": {
                "favorite_horse_median_finish_order": median_finish_order,
                "favorite_horse_win_total": favorite_horse_win_total,
                "favorite_horse_lost_total": favorite_horse_lost_total,
                "favorite_horse_winning_ratio": favorite_horse_winning_ratio,
            }
        }

    def summarize_race(self, race):
        race_name = race["name"] if "name" in race else race["id"]
        race_number = race["number"]
        
        horse_results = []
        for start in race["starts"]: 
            if "finishOrder" not in start["result"]:
                # Skip horses that did not finish or were disqualified
                continue

            final_odds = start["result"]["finalOdds"]
            if final_odds == 0:
                # Skip horses that lacks odds
                continue

            horse_name = start["horse"]["name"]
            finish_order = start["result"]["finishOrder"]

            horse_results.append({
                "horse_name": horse_name,
                "final_odds": final_odds,
                "finish_order": finish_order
            })

        top_3 = sorted(horse_results, key=lambda x: x["final_odds"])[:3]

        favorite_horse = top_3[0]
        favorite_horse_finish_order = favorite_horse["finish_order"]
        favorite_horse_winned = favorite_horse["finish_order"] == 1

        return {
            "race_name": race_name,
            "race_number": race_number,
            "top_3": top_3,
            "favorite_horse_winned": favorite_horse_winned,
            "favorite_horse_finish_order": favorite_horse_finish_order,
        }
