# Favorite horses

Streamlit app to analyse and visualize the data of the favorite horses in the recent ATG games.

App deployed at: [https://favorite-horses-wqamk6hs2a-ey.a.run.app/](https://favorite-horses-wqamk6hs2a-ey.a.run.app/)