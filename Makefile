PYTHON_VERSION=3.11
VIRTUALENV_NAME=atg

docker/build: 
	docker build .

pip/install:
	pip install -r requirements.txt

.PHONY: virtualenv/create
virtualenv/create:
	pyenv virtualenv $(PYTHON_VERSION) $(VIRTUALENV_NAME)

.PHONY: virtualenv/activate
virtualenv/activate:
	@echo "To activate please run the following command in your shell: 'pyenv activate $(VIRTUALENV_NAME)'"

.PHONY: virtualenv/delete
virtualenv/delete:
	pyenv virtualenv-delete $(VIRTUALENV_NAME)
