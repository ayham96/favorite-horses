import requests


class ATGClient:
    def __init__(self):
        self.base_url = "https://www.atg.se/services/racinginfo/v1/api"

    def get_games_by_type(self, game_type):
        url = f"{self.base_url}/products/{game_type}"
        response = requests.get(url)
        return response.json()

    def get_game_by_id(self, game_id):
        url = f"{self.base_url}/games/{game_id}"
        response = requests.get(url)
        return response.json()
