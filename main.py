import pandas as pd
import matplotlib.pyplot as plt
import streamlit as st

from client import ATGClient
from service import favoritesService

service = favoritesService(ATGClient())

def main():
    st.set_page_config(layout="wide")

    st.title("ATG favorites")

    col1, _ = st.columns([1, 4])  # Adjust the ratio as needed
    game_types = ["V75", "V86", "dd"]
    with col1:
        selected_game_type = st.selectbox('Select a game type:', game_types)

    json_data = service.summarize_last_3_games(selected_game_type)

    for game in json_data:
        st.header(f"Game: {game['game_id']}")

        st.subheader("Statistics:")

        # Plot the favorite horse median finish order
        X = list(map(lambda x: x["race_number"], game["races"]))
        Y = list(map(lambda x: x["favorite_horse_finish_order"], game["races"]))
        median  = game['statistics']['favorite_horse_median_finish_order']
        fig, ax = plt.subplots()
        ax.bar(X, Y, color='lightblue')
        ax.axhline(median, color='red', linestyle='dashed', linewidth=1, label=f'Median: {median:.2f}')
        ax.set_xlabel('Race Number')
        ax.set_ylabel('Favorite Horse Finish Order')
        ax.set_title('Favorite horse median finish order across all races')
        ax.legend()

        # Plot the winning ratio of the favorite horse
        categories = 'Won', 'Lost'
        sizes = [game["statistics"]["favorite_horse_win_total"], game["statistics"]["favorite_horse_lost_total"]]

        fig1, ax1 = plt.subplots()
        ax1.pie(sizes, labels=categories, autopct='%1.1f%%', startangle=90)
        ax1.set_title('Favorite horse winning ratio across all races')
        ax1.axis('equal')

        # Render the plots
        _, col1, _ ,col2, _ = st.columns([1, 2, 1, 2, 1]) 
        with col1:
            st.pyplot(fig)
        with col2:
            st.pyplot(fig1)


        st.subheader("Legs breakdown:")
        for race in game["races"]:
            st.markdown(f"### {race['race_number']} - {race['race_name']}")
            st.markdown("#### Favorite 3 horses in the race:")
            st.dataframe(pd.DataFrame(race["top_3"]), width=1000)
            st.write(f"Favorite horse won: {race['favorite_horse_winned']}")
        st.markdown("---")

if __name__ == "__main__":
    main()